import uuidv4 from "uuid/v4";

let comments = [
  {
    id: "123",
    text: "such a good question",
    post: "024",
    author: "1"
  },
  {
    id: "345",
    text: "Thank you for the idea",
    post: "012",
    author: "2"
  },
  {
    id: "678",
    text: "This worked well for me",
    post: "024",
    author: "2"
  }
];

let users = [
  {
    id: "1",
    email: "hmem@example",
    name: "hamza",
    age: 25
  },
  {
    id: "2",
    email: "Make@example",
    name: "Make",
    age: 26
  },
  {
    id: "3",
    email: "Sara@example",
    name: "Sara",
    age: 32
  }
];

let posts = [
  {
    id: "024",
    title: "graphql",
    body: "kashkdahskjd",
    published: true,
    author: "1"
  },
  {
    id: "13",
    title: "Gql 201",
    body: "This is an advanced",
    published: false,
    author: "1"
  },
  {
    id: "012",
    title: "Programming Music",
    body: "",
    published: false,
    author: "2"
  }
];

const resolvers = {
  Query: {
    users(parent, args, ctx, info) {
      if (!args.query) {
        return users;
      }

      return users.filter(user =>
        user.name.toLowerCase().includes(args.query.toLowerCase())
      );
    },
    posts(parent, args, ctx, info) {
      if (!args.query) {
        return posts;
      }

      return posts.filter(
        post =>
          post.title.toLowerCase().includes(args.query.toLowerCase()) ||
          post.body.toLowerCase().includes(args.query.toLowerCase())
      );
    },
    comments(parent, args, ctx, info) {
      return comments;
    },
    me() {
      return {
        id: "32653",
        name: "hamza hemm",
        email: "hamza@example.fr"
      };
    },
    post() {
      return {
        id: "092",
        title: "GQl",
        body: "KJSODICJSD",
        published: false
      };
    }
  },
  // ****************** Mutations ******************** //
  Mutation: {
    createUser(parent, { data }, cntx, info) {
      const emailTaken = users.some(user => user.email === data.email);

      if (emailTaken) {
        throw new Error("Email Taken!");
      }

      const user = {
        id: uuidv4(),
        ...data
      };

      console.log(user);

      users.push(user);

      return user;
    },
    createPost(parent, { data }, ctnx, info) {
      const userExist = users.some(user => user.id === data.author);

      if (!userExist) {
        throw new Error("User doesn't exist!!");
      }

      const post = {
        id: uuidv4(),
        ...data
      };

      posts.push(post);

      return post;
    },
    createComment(parent, { data }, cntx, info) {
      const userExist = users.some(user => user.id === data.author);
      const publishedPost = posts.some(
        post => post.id === data.post && post.published
      );

      if (!userExist || !publishedPost) {
        throw new Error(
          "user doesn't exist or the post doesn't published yet!!"
        );
      }

      const comment = {
        id: uuidv4(),
        ...data
      };

      comments.push(comment);

      return comment;
    },
    deleteUser(parent, args, context, info) {
      const userIndex = users.findIndex(user => user.id === args.id);

      if (userIndex === -1) {
        throw new Error("User doesn't exist!");
      }

      const deletedUsers = users.splice(userIndex, 1);

      posts = posts.filter(post => {
        const match = post.author === args.id;

        if (match) {
          // comments = comments.filter(comment => comment.post === post.id);
        }

        return !match;
      });

      comments = comments.filter(comment => comment.author !== args.id);

      return deletedUsers[0];
    },
    deletePost(parent, args, context, info) {
      const postIndex = posts.findIndex(post => post.id === args.id);

      if (postIndex === -1) {
        throw new Error("Post Not found!");
      }
      const deletedPosts = posts.splice(postIndex, 1);

      comments = comments.filter(comment => comment.post !== args.id);

      return deletedPosts[0];
    },
    deleteComment(parent, args, context, info) {
      const commentIndex = comments.findIndex(com => com.id === args.id);

      if (commentIndex === -1) {
        throw new Error("comment Not Found!");
      }
    }
  },
  Post: {
    author(parent, args, ctx, info) {
      return users.find(user => user.id === parent.author);
    },
    comments(parent, args, ctx, info) {
      return comments.filter(comment => comment.post === parent.id);
    }
  },
  Comment: {
    author(parent, args, context, info) {
      return users.find(user => user.id === parent.author);
    },
    post(parent, args, context, info) {
      return posts.find(post => post.id === parent.post);
    }
  },
  User: {
    posts(parent, args, ctx, info) {
      return posts.filter(post => post.author === parent.id);
    },
    comments(parent, args, ctx, info) {
      return comments.filter(comment => comment.author === parent.id);
    }
  }
};

export { resolvers as default };
