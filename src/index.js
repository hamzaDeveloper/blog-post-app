import { GraphQLServer } from "graphql-yoga";

import resolvers from "./GQL/resolvers";

import typeDefs from "./GQL/typeDefs";

const server = new GraphQLServer({
  typeDefs: "./src/GQL/schema.graphql",
  resolvers
});

server
  .start(() => console.log("the server is ready!!!"))
  .catch(err => console.log(err));
