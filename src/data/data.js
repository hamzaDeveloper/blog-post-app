

let users = [
  {
    id: "1",
    email: "hmem@example",
    name: "hamza",
    age: 25
  },
  {
    id: "2",
    email: "Make@example",
    name: "Make",
    age: 26
  },
  {
    id: "3",
    email: "Sara@example",
    name: "Sara",
    age: 32
  }
];

let posts = [
  {
    id: "024",
    title: "graphql",
    body: "kashkdahskjd",
    published: true,
    author: "1"
  },
  {
    id: "13",
    title: "Gql 201",
    body: "This is an advanced",
    published: false,
    author: "1"
  },
  {
    id: "012",
    title: "Programming Music",
    body: "",
    published: false,
    author: "2"
  }
];
export { users, comments, posts };
